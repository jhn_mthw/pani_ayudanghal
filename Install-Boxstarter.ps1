Set-ExecutionPolicy Bypass -Scope Process -Force;

# Install Boxstarter
if($PSVersionTable.PSVersion.Major -gt 3)
{
. { iwr -useb http://boxstarter.org/bootstrapper.ps1 } | iex; get-boxstarter -Force
}
else
{
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://boxstarter.org/bootstrapper.ps1')); Get-Boxstarter -Force
}

# Installing tools
Install-BoxstarterPackage -PackageName https://bitbucket.org/!api/2.0/snippets/jhn_mthw/EbGeyg/8a144aa977df3e7c6f4ed685c121a7a80c4d422b/files/Boxstarter-Script -DisableReboots