@echo off
:: RegEdit can only export into a single file at a time, so create two temporary files.
regedit /e "%OneDriveCommercial%\environment-backup1.reg" "HKEY_CURRENT_USER\Environment"
regedit /e "%OneDriveCommercial%\environment-backup2.reg" "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment"

:: Concatenate into a single file and remove temporary files.
type "%OneDriveCommercial%\environment-backup1.reg" "%OneDriveCommercial%\environment-backup2.reg" > "%OneDriveCommercial%\environment-variables-backup.reg"
del "%OneDriveCommercial%\environment-backup1.reg"
del "%OneDriveCommercial%\environment-backup2.reg"